from datetime import datetime
import time
import traceback
from mpi4py import MPI
from skopt import dump
import logging

# class to receive requests
class Receiver:

    # constructor
    def __init__(self):
        self.comm = MPI.COMM_WORLD
        self.stat = MPI.Status()
        self.nRank = self.comm.Get_rank()
        self.totalRanks = self.comm.Get_size()
        self.selectSource = MPI.ANY_SOURCE

    def getRank(self):
        """ Get rank of the `Receiver`
        """
        return self.comm.Get_rank() if self.comm else self.nRank

    def receiveRequest(self):
        """ Listen to request from any `Requester`.

            `mpi4py.send()` is called.
            Return True and request message if succeed else return False and an error message.

            Parameters:
            ----------
            No parameters are requried.
        """
        # wait for a request from any ranks
        while not self.comm.Iprobe(source=self.selectSource, status=self.stat):
            time.sleep(0.0001)

        try:
            # get the request
            reqData = self.comm.recv(source=self.stat.Get_source())
            return True, reqData
        except:
            errMsg = f'failed to got proper request with: {traceback.format_exc()}'
            return False, errMsg

    def getRequesterRank(self):
        """ Get rank of `Requester` for response to reach to.

            Return rank of `Requester`.

            Parameters:
            ----------
            No parameters are requried.
        """
        return self.stat.Get_source() if self.stat else self.nRank

    def returnResponse(self, rData, dest=0):
        """ Send response when receiving a request from `Requester`.

            `mpi4py.send()` is called.
            Return True and None if succeed else return False and an error message.

            Parameters:
            ----------
            * `rData` [unspecific type]:
                Response message.
            * `dest` [int, default=0]:
                Destination of response.
        """
        try:
            self.comm.send(rData, dest=(self.getRequesterRank() if dest == 0 else dest) )
            return True, None
        except:
            errMsg = f'failed to return response with: {traceback.format_exc()}'
            return False, errMsg

    # decrement nRank
    def decrementNumRank(self):
        """ Decrement number of active nodes.

            No return.

            Parameters:
            ----------
            No parameters are requried.
        """
        self.totalRanks -= 1

    # check if there is active worker rank
    def activeRanks(self):
        """ Get active nodes except self.

            Return total active nodes - 1 [int].

            Parameters:
            ----------
            No parameters are requried.
        """
        return self.totalRanks > 1

    def getTotalRanks(self):
        """ Get total ranks.

            Return total ranks [int].

            Parameters:
            ----------
            No parameters are requried.
        """
        return self.totalRanks

    def sendMessage(self, rData):
        """ Broadcast a message to rank > 0.

            `mpi4py.send()` is called.
            Return True and message if succeed else return False and an error message.

            Parameters:
            ----------
            * `rData` [unspecified type]:
                Data that will be broadcasted.
        """
        try:
            for i in range(1, self.totalRanks):
                self.comm.send(rData, dest=i)
            return True, None
        except:
            errMsg = f'failed to retrun response with: {traceback.format_exc()}'
            return False, errMsg
    def disconnect(self):
        try:
            self.comm.Disconnect()
            return True, None
        except:
            errMsg = 'failed to disconnect with: %s' % traceback.format_exc()
            return False, errMsg


# class to send requests
class Requester:
    """ Run hyperparameter optimisation using mpi + skopt.

        A `Requester` runs on a worker node to request a hyperparameter point from the master node.
        Usually is instantiated when MPI.COMM_WORLD.Get_rank() != 0.


        Parameters
        ----------
        No parameters are requried.

        Attributes
        ----------
        * `comm` [MPI.COMM_WORLD]:
            MPI communicator.
    """
    # constructor
    def __init__(self):
        self.comm = MPI.COMM_WORLD

    def getRank(self):
        """ Get rank of the `Requester`.
        """
        return self.comm.Get_rank()

    def sendRequest(self, request='', dest=0, params=tuple(), i_point=0):
        """ Send a request to another rank.

            A dictionary containing `request`, `rank`, and `params` will be sent to rank=`dest` and wait for an answer to confirm.
            `mpi4py.send()` is called.
            `mpi4py.send()` is called.
            Return True and confirmation message if confirmed else return False and an error message.

            Parameters:
            ----------
            * `request` [string]:
                Type of request, choose from
                - `get_point`: `params` is not needed;
                - `return_val`: `params` is a tuple of the search point and objective evaluation.
            * `dest` [int, default=0]:
                Destination of the request message.
                Default is the master node (rank = 0).

            * `params` [tuple, default=()]:
                Result to send when `request` is `return_val`.
        """
        if request not in ['get_point', 'return_val']:
            errMsg = f'request type {request} is not supported'
            return False, errMsg
        try:
            # encode
            data = {'request': request,
                    'rank': self.getRank(),
                    'params': params,
                    'i_point': i_point}
            # send a request ro rank0
            self.comm.send(data, dest=dest)

            # wait for the answer from Rank 0
            while not self.comm.Iprobe(source=dest):
                time.sleep(0.001)

            # get the answer
            ansData = self.comm.recv(source=dest)
            return True, ansData
        except:
            errMsg = f'failed to send the request: {traceback.format_exc()}'
            return False, errMsg

    def waitMessage(self, dest=0):
        """ Wait a message from another rank.

            Listen to rank = `dest`.
            Return True and message if succeed else return False and an error message.

            Parameters:
            ----------
            * `dest` [int, default=0]:
                Destination of the request message.
                Default is the master node (rank = 0).
        """
        try:
            # wait for message from Rank 0
            if self.comm.Iprobe(source=dest):
                time.sleep(0.0001)

            # get the answer
            ansData = self.comm.Irecv(source=dest)
            return True, ansData
        except:
            errMsg = f'failed to receive msg with: {traceback.format_exc()}'
            return False, errMsg

    def disconnect(self):
        try:
            if self.comm:
                self.comm.Disconnect()
            return True, None
        except:
            errMsg = 'failed to disconnect with: %s' % traceback.format_exc()
            return False, errMsg

def mpi_run(optimizer, objective, objective_args = tuple(), total_points = range(10)):
    """ Run hyperparameter optimisation using mpi + skopt.
        Parameters
        ----------
        * `optimizer` [skopt.Optimizer]:
            An optimizer of skopt defined by a search space, initial points, etc.

        * `objective` [a function]:
            Self-defined objective function that returns evaluation given a point (a python list).
            Return value must be a length-2 tuple of search point (a python list) and function score.
            The search point may be found in `objective_args`.
        * `objective_args` [tuple]:
            A tuple of parameters for the `objective` function (except for the point)
            Most importantly the search point should be passed.
            Other customised parameters can simply be passed.

        * `total_points` [generator, default=range(10)]:
            Generator of steps for parameter search.
            Use generator to support checkpoint.
    """
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    logger = logging.getLogger('foo')
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.INFO)
    i_points = list(total_points)
    output_path = './mpi/'
    import socket
    print('hostname', socket.gethostname(), 'rank', rank)

    if rank == 0:
        import os
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        tell_point = i_points[0]
        recvr = Receiver()

        while True:
            msg = recvr.receiveRequest()

            if msg[1]['request'] == 'get_point':
                if len(i_points) > 0:
                    try:
                        logger.info(f'{datetime.now().strftime("%Y-%m-%d %H:%M:%S")} [rank0] was asked by rank{recvr.stat.Get_source()} to give a point, now i_point is {i_points}, request message is {msg}')

                        sampled_point = optimizer.ask()
                        args = (sampled_point, i_points[0]) + objective_args

                        status = recvr.returnResponse(args, dest=int(msg[1]['rank']))
                        assert(status[0]), status[1]
                        logger.info(f'{datetime.now().strftime("%Y-%m-%d %H:%M:%S")} [rank0] sent point args to rank{recvr.stat.Get_source()}, now i_point is {i_points[0]}')
                        i_points.pop(0)
                    except:
                        logger.info(f'{datetime.now().strftime("%Y-%m-%d %H:%M:%S")} [rank0] \033[0;37;41mwill return because could not send new point. Number of active nodes are {recvr.totalRanks}\033[00m')
                        break
                else:
                    status = recvr.returnResponse('done')
                    recvr.decrementNumRank()
                    logger.info(f'{datetime.now().strftime("%Y-%m-%d %H:%M:%S")} [rank0] decreased the number of active nodes to {recvr.totalRanks}')
            elif msg[1]['request'] == 'return_val':
                try:
                    logger.info(f'{datetime.now().strftime("%Y-%m-%d %H:%M:%S")} [rank0] received a value from rank{recvr.stat.Get_source()}, now i_point is {i_points[0] if len(i_points) else "empty"} {msg}')
                    recvr.returnResponse('I received your value')
                    logger.info(f'{datetime.now().strftime("%Y-%m-%d %H:%M:%S")} [rank0] sent messages to rank{recvr.stat.Get_source()}')
                    if msg[1]['i_point'] > 0:
                        opt_result = optimizer.tell(*msg[1]['params'])
                        # Save optimizer state and most recent results
                        with open(f'{output_path}/optimizer_ask{msg[1]["i_point"]}_tell{tell_point}.pkl', 'wb') as f:
                            dump(optimizer, f)
                        with open(f'{output_path}/results_ask{msg[1]["i_point"]}_tell{tell_point}.pkl', 'wb') as f:
                            if opt_result.specs is not None:
                                if 'func' in opt_result.specs['args']:
                                    res_without_func = copy.deepcopy(opt_result)
                                    del res_without_func.specs['args']['func']
                                    dump(res_without_func, f)
                                else:
                                    dump(opt_result, f)
                            else:
                                dump(opt_result, f)
                        tell_point += 1
                    if len(i_points) == 0:
                        status = recvr.returnResponse('done')

                except:
                    logger.info(f'{datetime.now().strftime("%Y-%m-%d %H:%M:%S")} [rank0] \033[0;37;41mwill return because could not send confirmation after receiving training result. Number of active nodes are {recvr.totalRanks}\033[00m')
                    assert(0)
            if not recvr.activeRanks():
                time.sleep(10)
                logger.info(f'{datetime.now().strftime("%Y-%m-%d %H:%M:%S")} [rank0] will return because no active workers. Number of active nodes are {recvr.totalRanks}')
                break
    else:
        rqstr = Requester()

        while True:
            status, msg = rqstr.sendRequest(request = 'get_point')
            logger.info(f'{datetime.now().strftime("%Y-%m-%d %H:%M:%S")} [rank{rqstr.getRank()}] received `{msg}` when request next point')
            assert(status), 'no points received'
            if msg == 'done':
                logger.info(f'[rank{rqstr.getRank()}] I will return because I cannot get new points')
                import gc
                gc.collect()
                rqstr.disconnect()
                exit()
            logger.info(f'{datetime.now().strftime("%Y-%m-%d %H:%M:%S")} [rank{rqstr.getRank()}] will run objective with arg = {msg}')
            f_val = objective(tuple(msg[0]))
            logger.info(f'{datetime.now().strftime("%Y-%m-%d %H:%M:%S")} [rank{rqstr.getRank()}] will send {f_val} back')
            status, confirmation = rqstr.sendRequest(request = 'return_val', params = f_val, i_point = msg[1]) # msg[1] is i_point
            logger.info(f'{datetime.now().strftime("%Y-%m-%d %H:%M:%S")} [rank{rqstr.getRank()}] received `{confirmation}` after sending evaluation')

def check_results(ask_id, tell_id):
    from skopt import load
    output_path = './mpi/'
    res_loaded = load(f'{output_path}/results_ask{ask_id}_tell{tell_id}.pkl')
    print(res_loaded)
    print('zhangr fun', res_loaded['func_vals'])
    print('zhangr x', len(res_loaded['x_iters']))

def load_optimizer(ask_id, tell_id):
    from skopt import load
    output_path = './mpi/'
    opt_loaded = load(f'{output_path}/optimizer_ask{ask_id}_tell{tell_id}.pkl')
    return opt_loaded
